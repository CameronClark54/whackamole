#include "Mole.h"
#include <cstdlib>



Mole::Mole(std::vector<sf::Texture>& moleTextures, sf::Vector2u screenSize)
{
	//3x3 grid can be done using a vector or manually positioned, use a rand to select where the mole pops up, need to add an extra timer to despawn moles
	

	int chosenIndex = rand()%9;
	sprite.setTexture(moleTextures[0]);

	pointsValue = 200;//picks the amount of points the item is worth using the random number



	if (chosenIndex == 0) 
	{
		
		//chooses random x and y positions for items to spawn, reduced by the size of the texure to avoid spawning off screen
		int positionX =  (screenSize.x / 2 - moleTextures[0].getSize().x / 2);
		int positionY = (screenSize.y / 2 - moleTextures[0].getSize().y / 2);
		//sets position with x and y positions
		sprite.setPosition(positionX, positionY);
		
	}

	if (chosenIndex == 1)
	{

		//chooses random x and y positions for items to spawn, reduced by the size of the texure to avoid spawning off screen
		int positionX = (screenSize.x / 2 - moleTextures[0].getSize().x / 2 - 175);
		int positionY = (screenSize.y / 2 - moleTextures[0].getSize().y / 2);
		//sets position with x and y positions
		sprite.setPosition(positionX, positionY);

	}

	if (chosenIndex == 2)
	{

		//chooses random x and y positions for items to spawn, reduced by the size of the texure to avoid spawning off screen
		int positionX = (screenSize.x / 2 - moleTextures[0].getSize().x / 2 + 175);
		int positionY = (screenSize.y / 2 - moleTextures[0].getSize().y / 2);
		//sets position with x and y positions
		sprite.setPosition(positionX, positionY);

	}

	if (chosenIndex == 3)
	{

		//chooses random x and y positions for items to spawn, reduced by the size of the texure to avoid spawning off screen
		int positionX = (screenSize.x / 2 - moleTextures[0].getSize().x / 2);
		int positionY = (screenSize.y / 2 - moleTextures[0].getSize().y / 2 - 175);
		//sets position with x and y positions
		sprite.setPosition(positionX, positionY);

	}

	if (chosenIndex == 4)
	{

		//chooses random x and y positions for items to spawn, reduced by the size of the texure to avoid spawning off screen
		int positionX = (screenSize.x / 2 - moleTextures[0].getSize().x / 2 - 175);
		int positionY = (screenSize.y / 2 - moleTextures[0].getSize().y / 2 - 175);
		//sets position with x and y positions
		sprite.setPosition(positionX, positionY);

	}

	if (chosenIndex == 5)
	{

		//chooses random x and y positions for items to spawn, reduced by the size of the texure to avoid spawning off screen
		int positionX = (screenSize.x / 2 - moleTextures[0].getSize().x / 2 + 175);
		int positionY = (screenSize.y / 2 - moleTextures[0].getSize().y / 2 - 175);
		//sets position with x and y positions
		sprite.setPosition(positionX, positionY);

	}

	if (chosenIndex == 6)
	{

		//chooses random x and y positions for items to spawn, reduced by the size of the texure to avoid spawning off screen
		int positionX = (screenSize.x / 2 - moleTextures[0].getSize().x / 2);
		int positionY = (screenSize.y / 2 - moleTextures[0].getSize().y / 2 + 175);
		//sets position with x and y positions
		sprite.setPosition(positionX, positionY);

	}

	if (chosenIndex == 7)
	{

		//chooses random x and y positions for items to spawn, reduced by the size of the texure to avoid spawning off screen
		int positionX = (screenSize.x / 2 - moleTextures[0].getSize().x / 2 - 175);
		int positionY = (screenSize.y / 2 - moleTextures[0].getSize().y / 2 + 175);
		//sets position with x and y positions
		sprite.setPosition(positionX, positionY);

	}

	if (chosenIndex == 8)
	{

		//chooses random x and y positions for items to spawn, reduced by the size of the texure to avoid spawning off screen
		int positionX = (screenSize.x / 2 - moleTextures[0].getSize().x / 2 + 175);
		int positionY = (screenSize.y / 2 - moleTextures[0].getSize().y / 2 + 175);
		//sets position with x and y positions
		sprite.setPosition(positionX, positionY);

	}
	
}
