#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Mole.h"
#include "Player.h"
#include <vector>
#include <cstdlib>
#include <time.h>
#include <string>

int main()
{
	// declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Whack A Mole", sf::Style::Titlebar | sf::Style::Close);

	//
	//GAME SETUP
	//

	//hole texture
	sf::Texture holeTexture;
	holeTexture.loadFromFile("Assets/Graphics/hole.png");

	//set holes
	sf::Sprite holeSprite1;
	holeSprite1.setTexture(holeTexture);
	holeSprite1.setPosition(gameWindow.getSize().x / 2 - holeTexture.getSize().x / 2, gameWindow.getSize().y / 2 - holeTexture.getSize().y / 2);

	sf::Sprite holeSprite2;
	holeSprite2.setTexture(holeTexture);
	holeSprite2.setPosition(gameWindow.getSize().x / 2 - holeTexture.getSize().x / 2 - 175, gameWindow.getSize().y / 2 - holeTexture.getSize().y / 2);

	sf::Sprite holeSprite3;
	holeSprite3.setTexture(holeTexture);
	holeSprite3.setPosition(gameWindow.getSize().x / 2 - holeTexture.getSize().x / 2 + 175, gameWindow.getSize().y / 2 - holeTexture.getSize().y / 2);

	sf::Sprite holeSprite4;
	holeSprite4.setTexture(holeTexture);
	holeSprite4.setPosition(gameWindow.getSize().x / 2 - holeTexture.getSize().x / 2, gameWindow.getSize().y / 2 - holeTexture.getSize().y / 2 + 175);

	sf::Sprite holeSprite5;
	holeSprite5.setTexture(holeTexture);
	holeSprite5.setPosition(gameWindow.getSize().x / 2 - holeTexture.getSize().x / 2 - 175, gameWindow.getSize().y / 2 - holeTexture.getSize().y / 2 + 175);

	sf::Sprite holeSprite6;
	holeSprite6.setTexture(holeTexture);
	holeSprite6.setPosition(gameWindow.getSize().x / 2 - holeTexture.getSize().x / 2 + 175, gameWindow.getSize().y / 2 - holeTexture.getSize().y / 2 + 175);

	sf::Sprite holeSprite7;
	holeSprite7.setTexture(holeTexture);
	holeSprite7.setPosition(gameWindow.getSize().x / 2 - holeTexture.getSize().x / 2, gameWindow.getSize().y / 2 - holeTexture.getSize().y / 2 - 175);

	sf::Sprite holeSprite8;
	holeSprite8.setTexture(holeTexture);
	holeSprite8.setPosition(gameWindow.getSize().x / 2 - holeTexture.getSize().x / 2 - 175, gameWindow.getSize().y / 2 - holeTexture.getSize().y / 2 - 175);

	sf::Sprite holeSprite9;
	holeSprite9.setTexture(holeTexture);
	holeSprite9.setPosition(gameWindow.getSize().x / 2 - holeTexture.getSize().x / 2 + 175, gameWindow.getSize().y / 2 - holeTexture.getSize().y / 2 - 175);
	

	// Player Sprite
	sf::Texture playerTexture;
	// load our texture using a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	//declare player object
	Player playerObject(playerTexture, gameWindow.getSize());
	sf::Sprite playerSprite;


	// Moles
	// loads all the textures that will be used by items
	std::vector<sf::Texture> moleTextures;
	moleTextures.push_back(sf::Texture());


	moleTextures[0].loadFromFile("Assets/Graphics/mole.png");


	// create a vector to hold our items
	std::vector<Mole> moles;

	// loads up some starting items
	moles.push_back(Mole(moleTextures, gameWindow.getSize()));

	sf::Time moleUpTime = sf::seconds(3.0f);
	sf::Time timeLeft = moleUpTime;
	


	// create a time value that stores the total time between each item spawn
	sf::Time moleSpawnDuration = sf::seconds(3.0f);
	// create a timer that stores the time remaining for our game
	sf::Time moleSpawnRemaining = moleSpawnDuration;


	// Game Music
	sf::Music gameMusic;
	// load our audio using a file path
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	// start the music
	gameMusic.play();

	// Sounds
	//load a sound effect file into a soundBuffer
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");
	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");
	//setup a Sound object to play the sound later and associate it with the SoundBuffer
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);
	sf::Sound victorySound;
	victorySound.setBuffer(victorySoundBuffer);

	// Game Font
	sf::Font gameFont;
	// load our font using a file path
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");


	// Title Text
	// declare a text variable called titleText to hold our gametitle
	sf::Text titleText;
	// set the font our text will be
	titleText.setFont(gameFont);
	// set what will be displayed by the text 
	titleText.setString("WHACK A MOLE");
	// set the size of our text, in pixels
	titleText.setCharacterSize(24);
	// set the colour of our text
	titleText.setFillColor(sf::Color::Black);
	// set the text style
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// positions the text in the top center of the screen
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);


	// Author Text
	sf::Text authorText;
	authorText.setFont(gameFont);
	authorText.setString("Created by Cameron Clark");
	authorText.setCharacterSize(16);
	authorText.setFillColor(sf::Color::Magenta);
	authorText.setStyle(sf::Text::Italic);
	authorText.setPosition(gameWindow.getSize().x / 2 - authorText.getLocalBounds().width / 2, 60);

	//Timer
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);
	sf::Time timeLimit = sf::seconds(60.0f);
	sf::Time timeRemaining = timeLimit;
	sf::Clock gameClock;

	srand(time(NULL));


	//Score
	//set varible and initailize it
	int score = 0;

	//setup score text 
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(30, 30);

	//Player movement
	sf::Vector2f playerVelocity(0.0f, 0.0f);
	float speed = 100.0f;

	//game over variable
	bool gameOver = false;

	// Game Over Text
	//declare a text variable to hold our game over display
	sf::Text gameOverText;
	//set the font our text should use
	gameOverText.setFont(gameFont);
	//set message
	gameOverText.setString("GAME OVER");
	//set message size in pixels
	gameOverText.setCharacterSize(72);
	//set text colour
	gameOverText.setFillColor(sf::Color::Cyan);
	//set text style
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	//set text position
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	//restart text
	sf::Text restartText;
	//set the font our text should use
	restartText.setFont(gameFont);
	//set message
	restartText.setString("PRESS R TO RESTART");
	//set message size in pixels
	restartText.setCharacterSize(72);
	//set text colour
	restartText.setFillColor(sf::Color::Cyan);
	//set text style
	restartText.setStyle(sf::Text::Bold | sf::Text::Italic);
	//set text position
	restartText.setPosition(gameWindow.getSize().x / 2 - restartText.getLocalBounds().width / 2, 300);

	//quit text
	sf::Text quitText;
	//set the font our text should use
	quitText.setFont(gameFont);
	//set message
	quitText.setString("PRESS Q TO QUIT");
	//set message size in pixels
	quitText.setCharacterSize(72);
	//set text colour
	quitText.setFillColor(sf::Color::Cyan);
	//set text style
	quitText.setStyle(sf::Text::Bold | sf::Text::Italic);
	//set text position
	quitText.setPosition(gameWindow.getSize().x / 2 - quitText.getLocalBounds().width / 2, 450);

	//Game Loop
	while (gameWindow.isOpen())
	{
		//
		// Input Section
		//

		//declare a variable to hold an event
		sf::Event gameEvent;

		while (gameWindow.pollEvent(gameEvent))
		{
			//check if the player has tried to close the game window
			if (gameEvent.type == sf::Event::Closed)
			{
				//if true then close the game window
				gameWindow.close();

			}//end if


		}//end loop

		//player keyboard input
		playerObject.Input();


		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			// Reset the game
			score = 0;
			timeRemaining = timeLimit;
			moles.clear();//.clear removes all items in a vector
			moles.push_back(Mole(moleTextures, gameWindow.getSize()));
			gameMusic.play();
			gameOver = false;
			playerObject.Reset(gameWindow.getSize());
		}

		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			gameWindow.close();
		}


		//
		//Update game Section
		//

		//Update game state

		sf::Time frameTime = gameClock.restart();
		timeRemaining = timeRemaining - frameTime;

		//check if time has run out
		if (timeRemaining.asSeconds() <= 0)
		{
			//timer = 0
			timeRemaining = sf::seconds(0);
			//preforms when game ends
			if (gameOver == false)
			{
				gameOver = true;
				//stop main music playing
				gameMusic.stop();
				//play victory sound
				victorySound.play();
			}
		}

		//update our timer based on our time remaining
		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));

		//update our score display based on our current score
		scoreText.setString("Score: " + std::to_string(score));

		if (!gameOver)
		{
			// update our item spawn time remaining based on how much time passed last frame
			moleSpawnRemaining =moleSpawnRemaining - frameTime;
			// check if time remaining to next spawn is 0
			if (moleSpawnRemaining <= sf::seconds(0.0f))
			{
				//spawn a new item
				moles.push_back(Mole(moleTextures, gameWindow.getSize()));
				//reset time remaining
				moleSpawnRemaining = moleSpawnDuration;
				
			}

			//move sprite
			playerObject.Update(frameTime);

			//check for collisions
			//first loop goes backwards through the data
			for (int i = moles.size() - 1; i >= 0; --i)
			{
				sf::FloatRect itemBounds = moles[i].sprite.getGlobalBounds();
				sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();

				if (itemBounds.intersects(playerBounds))
				{
					//player touched an item
					score += moles[i].pointsValue;//gives points
					moles.erase(moles.begin() + i);//removes item
					pickupSound.play();//plays pickup sound
				}

				//used to delete items based on time
				moleUpTime = moleUpTime - frameTime;
				if (moleUpTime <= sf::seconds(0.0f)) 
				{
					moles.erase(moles.begin() + i);//removes item
					moleUpTime = timeLeft;
				}
			}
		}


		//
		//Draw section
		//

		//clears the window to a single colour
		gameWindow.clear(sf::Color::Green);



		//draws everything to the window
		gameWindow.draw(titleText);
		gameWindow.draw(authorText);
		gameWindow.draw(scoreText);
		gameWindow.draw(timerText);
		gameWindow.draw(holeSprite1);
		gameWindow.draw(holeSprite2);
		gameWindow.draw(holeSprite3);
		gameWindow.draw(holeSprite4);
		gameWindow.draw(holeSprite5);
		gameWindow.draw(holeSprite6);
		gameWindow.draw(holeSprite7);
		gameWindow.draw(holeSprite8);
		gameWindow.draw(holeSprite9);

		if (!gameOver)
		{
			gameWindow.draw(playerObject.sprite);
			

			//draws items
			for (int i = 0; i < moles.size(); ++i)
			{
				gameWindow.draw(moles[i].sprite);
			}
		}

		if (gameOver)
		{
			gameWindow.draw(gameOverText);
			gameWindow.draw(restartText);
			gameWindow.draw(quitText);
		}

		// displays the windows contents onto the screen
		gameWindow.display();
	}//end of game loop



	return 0;
}//end of main()